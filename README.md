# devops-school-01

## Задание

Необходимо запустить airflow с мониторингом. Мониторинг должен покрывать airflow, prometheus, grafana, машину на которой запущен docker (node exporter)
1. airflow
2. prometheus
3. grafana
4. node exporter

Нужно написать dag, который будет получать погоду с любого сервиса и выводить его в свои логи, чтобы было видно в airflow. город должен задаваться через параметр.

## Развертывание

Запуск производится в Docker Compose командой 
```bash
docker compose up -d
```

После запуска становятся доступны сервисы:

| Порт | Сервис |
|---|---|
| [8080](http://localhost:8080) | Airflow |
| [8081](http://localhost:8081/?orgId=1) | Grafana |
| [9090](http://localhost:9090) | Prometheus |
| [9100](http://localhost:9100) | Node exporter |

### Мониторинг

#### Prometheus

Файл конфигурации Prometheus .config_files\prometheus\prometheus.yml
```yml
# my global config
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.
  evaluation_interval: 15s # By default, scrape targets every 15 seconds.
  # scrape_timeout is set to the global default (10s).

# Load and evaluate rules in this file every 'evaluation_interval' seconds.
rule_files:
  - 'alerts/*.rules'

# alert
alerting:
  alertmanagers:
  - scheme: http
    static_configs:
    - targets:
      - 'alertmanager:9093'

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'
    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 15s
    static_configs:
      - targets:
        - 'localhost:9090'

  - job_name: 'statsd-exporter'
    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 15s
    static_configs:
      - targets: 
        - 'statsd-exporter:9102'

  - job_name: 'node-exporter'
    scrape_interval: 15s
    static_configs:
      - targets:
        - 'node-exporter:9100'

  - job_name: postgres-exporter
    static_configs:
      - targets: ["postgres-exporter:9187"]
```
#### Grafana
Каталог .config_files\grafana\etc\grafana\provisioning\datasources\ содежит источники данных prometheus для grafana
Каталог .config_files\grafana\var\lib\grafana\dashboards\ содержит информационные панели grafana
```yml
datasources:
- name: 'prometheus'
  url: 'http://prometheus:9090'
  editable: true
  is_default: true
  type: 'prometheus'
  version: 1
  org_id: 1
```
Dashboards
* Airflow DAG dashboard
![Airflow DAG dashboard](images/Dashboards/DAG.png)
* Airflow cluster dashboard
![Airflow_cluster_dashboard](images/Dashboards/Airflow_cluster_dashboard.png)
* Grafana
![Grafana](images/Dashboards/Grafana.png)
* Node Exporter
![Node Exporter](images/Dashboards/Node_Exporter.png)
* PostgreSQL
![PostgreSQL](images/Dashboards/PostgreSQL.png)
* Prometheus
![Prometheus](images/Dashboards/Prometheus.png)

### Airflow DAG
Каталог .dags содержит Airflow DAG.
Каталог .bash содержит bash скрипты.

DAG weather_bash.py  в 00.00 каждые сутки запускает bash скрипт script.sh, который делает запрос текущей температуры на сайте https://open-meteo.com/ и обрабатывает его. Координаты месности задаются через переменные lat и long скрипта script.sh. Координаты можно узнать по адресу https://open-meteo.com/en/docs

DAG weather_bash.py
```python
from airflow import DAG
from datetime import timedelta

from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
from datetime import datetime

# Variable
bash_script = "/opt/airflow/bash/script.sh "
lat = "{{ 56.2624 }}"
long = "{{ 34.3282 }}"

weather_bash = DAG(
    "weather_bash", 
    start_date=days_ago(0, 0, 0, 0,0)
          )


operation = BashOperator(
    bash_command = bash_script,
    dag=weather_bash,
    task_id='operation'
)

operation
```
script.sh
```shell
#!/bin/bash
lat=$"56.2624"
long=$"34.3282"
request=$(curl -s "https://api.open-meteo.com/v1/forecast?latitude=$lat&longitude=$long&current=temperature_2m&timezone=Europe%2FMoscow")

cut_text () {
echo $request |  sed 's/^.*'$1'//;s/'$2'.*$//'
}

date=$(cut_text 'rent":{"time":"' 'T')
time=$(cut_text 'T' '","interval":9')
temp=$(cut_text '0,"temperature_2m":' '}}')
echo "========================================================="
echo "Area coordinates: "$lat" : "$long
echo "Date: " $date "Time: " $time
echo "Temperature: " $temp"°C"
echo "========================================================="
```

## Результат
* Результат работы Airflow DAG
![Result](images/Dashboards/Result.png)

