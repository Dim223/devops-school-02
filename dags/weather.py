from airflow import DAG
from datetime import timedelta

from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
from datetime import datetime

# Variable
lat = "{{ 56.2624 }}"
long = "{{ 34.3282 }}"

weather = DAG(
    "weather", 
    start_date=days_ago(0, 0, 0, 0,0)
          )


operation = BashOperator(
    bash_command = f'curl  "https://api.open-meteo.com/v1/forecast?latitude={lat}&longitude={long}&current=temperature_2m&timezone=Europe%2FMoscow&format=csv"',
    dag=weather,
    task_id='operation'
)


operation

