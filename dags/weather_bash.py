from airflow import DAG
from datetime import timedelta

from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
from datetime import datetime

# Variable
bash_script = "/opt/airflow/bash/script.sh "

weather_bash = DAG(
    "weather_bash", 
    start_date=days_ago(0, 0, 0, 0,0)
          )


operation = BashOperator(
    bash_command = bash_script,
    dag=weather_bash,
    task_id='operation'
)

operation
