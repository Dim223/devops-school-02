#!/bin/bash
lat=$"56.2624"
long=$"34.3282"
request=$(curl -s "https://api.open-meteo.com/v1/forecast?latitude=$lat&longitude=$long&current=temperature_2m&timezone=Europe%2FMoscow")

cut_text () {
echo $request |  sed 's/^.*'$1'//;s/'$2'.*$//'
}

date=$(cut_text 'rent":{"time":"' 'T')
time=$(cut_text 'T' '","interval":9')
temp=$(cut_text '0,"temperature_2m":' '}}')
echo "========================================================="
echo "Area coordinates: "$lat" : "$long
echo "Date: " $date "Time: " $time
echo "Temperature: " $temp"°C"
echo "========================================================="